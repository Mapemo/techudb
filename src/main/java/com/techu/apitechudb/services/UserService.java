package com.techu.apitechudb.services;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.ProductRepository;
import com.techu.apitechudb.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(int ageFilter) {
        System.out.println("findAll in ProductService");
        if (ageFilter <= 0) {
            return this.userRepository.findAll();
        }
        System.out.println("ageFilter es " + ageFilter);
        ArrayList<UserModel> result = new ArrayList<>();

        for (UserModel userinList : ApitechudbApplication.userModels) {
            if (userinList.getAge() == ageFilter) {
                System.out.println("Usuario" + userinList.getName() + "tiene misma edad que filtro");
                result.add(userinList);
            }
        }
        return result;
    }
    public UserModel add(UserModel user) {
        System.out.println("add en USERService");

        return this.userRepository.save(user);
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findByID enUSERtService");

        return this.userRepository.findById(id);
    }

    public UserModel update(UserModel userModel) {
        System.out.println("update en ProductService");
        return this.userRepository.update(userModel);
    }

    public boolean delete(String id) {
        System.out.println("Delete en ProductService");

        boolean result = false;

        Optional<UserModel> userToDelete = this.findById(id);

        if (userToDelete.isPresent() == true) {
            this.userRepository.delete(userToDelete.get());
        }
        return result;
    }

}