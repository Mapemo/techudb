package com.techu.apitechudb.services;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;

    public PurchaseServiceResponse addPurchase(PurchaseModel purchase) {
        System.out.println("add en PurcharseService");

        PurchaseServiceResponse result = new PurchaseServiceResponse();

        result.setPurchase(purchase);

        if (this.userService.findById(purchase.getUserID()).isPresent() == false) {
            System.out.println("El usuario de la compra no se ha encontrado");
            result.setMsg("El usuario de la compra no se ha encontrado");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }
        if (this.getById(purchase.getId()).isPresent() == true) {
            result.setMsg("Ya hay compra con esa ID");
            return result;
        }
        float amount = 0;
        for (Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()) {
            if (this.productService.findById(purchaseItem.getKey()).isPresent() == false) {
                System.out.println("El producto con la id " + purchaseItem.getKey() + " no se encuentra en el sistema");
                result.setMsg("El producto con la id " + purchaseItem.getKey() + " no se encuentra en el sistema");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
                return result;
            } else {
                System.out.println("Añadiendo valor de " + purchaseItem.getValue() + " unidades del producto total");
                amount += (this.productService.findById(purchaseItem.getKey()).get().getPrice()
                        * purchaseItem.getValue());
            }
        }

        purchase.setAmount(amount);
        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.OK);

        return result;
    }

    public List<PurchaseModel> getPurchases() {
        System.out.println("getPurchases en PurchaseRepository");

        return this.purchaseRepository.findAll();
    }

    public Optional<PurchaseModel> getById(String id) {
        System.out.println("getById en PurchaseService");
        System.out.println("La id es " + id);

        return this.purchaseRepository.findById(id);
    }

}
