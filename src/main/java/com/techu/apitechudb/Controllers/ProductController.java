package com.techu.apitechudb.Controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.productService.findAll(),
                HttpStatus.OK
        );


    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("Add Product ");
        System.out.println("La id del producto a crear es " + product.getId());
        System.out.println("La descripción del producto a crear es " + product.getDesc());
        System.out.println("El precio del producto a crear es " + product.getPrice());
        return new ResponseEntity<>(
                this.productService.add(product),
                HttpStatus.CREATED
        );
    }
    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductsById(@PathVariable String id) {
        System.out.println("GetProductsbyID");
        System.out.println("La id del producto a crear es " + id);

        Optional<ProductModel> result = this.productService.findById(id);

        return new ResponseEntity<>(
        result.isPresent() ? result.get() : "Producto no encontrado",
        result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("La id del producto a actualizar en parametro: " + id);
        System.out.println("Id product a actualizar:" + product.getId());
        System.out.println("Descripción product a actualizar: " + product.getDesc());
        System.out.println("Precio product a actual: " + product.getPrice());

        return new ResponseEntity<>(this.productService.update(product), HttpStatus.OK);

    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id) {
        System.out.println("Delete Product");
        System.out.println("La id dle producto a borrar: " + id);
        boolean deleteProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "Producto borrado" : "Producto no encontrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }
}
