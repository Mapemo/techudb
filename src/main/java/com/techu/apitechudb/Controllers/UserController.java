package com.techu.apitechudb.Controllers;



import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUser(
            @RequestParam(name = "ageFilter", required = false, defaultValue = "0") int ageFilter
    ) {
        System.out.println("getUsers");

        return new ResponseEntity<>(
                this.userService.findAll(ageFilter),
                HttpStatus.OK
        );


    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("Add Product ");
        System.out.println("La id del USUARIO a crear es " + user.getId());
        System.out.println("La descripción del USUARIO a crear es " + user.getName());
        System.out.println("El precio del USUARIO a crear es " + user.getAge());
        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }
    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUsersById(@PathVariable String id) {
        System.out.println("GetUSUARIObyID");
        System.out.println("La id del USUARIO a crear es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("La id del usuario a actualizar en parametro: " + id);
        System.out.println("Id usuario a actualizar:" + user.getId());
        System.out.println("Descripción usuario a actualizar: " + user.getName());
        System.out.println("Precio usuario a actual: " + user.getAge());

        return new ResponseEntity<>(this.userService.update(user), HttpStatus.OK);

    }


    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("La id USUARIO a borrar: " + id);
        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "USUARIO borrado" : "USUARIO no encontrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }
}