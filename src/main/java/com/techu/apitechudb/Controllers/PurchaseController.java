package com.techu.apitechudb.Controllers;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2")

public class PurchaseController {
    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases() {
        System.out.println("getPurchases");

        return new ResponseEntity<>(this.purchaseService.getPurchases(), HttpStatus.OK);
    }

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseModel> addPurchase (@RequestBody PurchaseModel purchase){
        System.out.println("addPurchase");
        System.out.println("La id de purchase a crear es " + purchase.getId());
        System.out.println("El userId de purchase a crear es " + purchase.getUserID());
        System.out.println("El amount de purchase a crear es " + purchase.getAmount());
        System.out.println("Los purchaseItems a crear son " + purchase.getPurchaseItems());

        PurchaseServiceResponse purchaseServiceResponse = this.purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(
                purchaseServiceResponse.getPurchase(),
                purchaseServiceResponse.getResponseHttpStatusCode()
        );
    }

}
