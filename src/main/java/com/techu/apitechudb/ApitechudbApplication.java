package com.techu.apitechudb;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import org.apache.catalina.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ApitechudbApplication {

	public static ArrayList<ProductModel> productModels;
	public static ArrayList<UserModel> userModels;
	public static ArrayList<PurchaseModel> purchaseModels;

	public static void main(String[] args) {

		SpringApplication.run(ApitechudbApplication.class, args);

		ApitechudbApplication.productModels = ApitechudbApplication.getTestData();
		ApitechudbApplication.userModels = ApitechudbApplication.getTestDatu();
		ApitechudbApplication.purchaseModels = ApitechudbApplication.getTestPurch();
	}

	private static ArrayList<ProductModel> getTestData() {
		ArrayList<ProductModel> productModels = new ArrayList<>();
		productModels.add(
				new ProductModel(
						"1"
						,"Producto 1"
						,10
				)
		);
		productModels.add(
				new ProductModel(
						"2"
						,"Producto 2"
						,20
				)
		);
		productModels.add(
				new ProductModel(
						"19"
						,"Producto 19"
						,199
				)
		);
		return productModels;
	}

	private static ArrayList<UserModel> getTestDatu() {
		ArrayList<UserModel> userModels = new ArrayList<>();
		userModels.add(
				new UserModel(
						"1"
						,"Usuario 1"
						,18
				)
		);
		userModels.add(
				new UserModel(
						"2"
						,"Usuario 2"
						,20
				)
		);
		userModels.add(
				new UserModel(
						"3"
						,"Usuario 3"
						,99
				)
		);
		userModels.add(
				new UserModel(
						"5"
						,"Usuario 5"
						,35
				)
		);
		return userModels;
	}
	private static ArrayList<PurchaseModel> getTestPurch() {
		return new ArrayList<>();
	}
}

