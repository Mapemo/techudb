package com.techu.apitechudb.models;

import java.util.Map;

public class PurchaseModel {
    public String id;
    public String userID;
    public float amount;
    private Map<String, Integer> purchaseItems;

    public PurchaseModel(String id, String userID, float amount) {
        this.id = id;
        this.userID = userID;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
    }

    public PurchaseModel() {

    }

    public String getId() {
        return id;
    }

    public String getUserID() {
        return userID;
    }

    public float getAmount() {
        return amount;
    }

    public Map<String, Integer> getPurchaseItems() {
        return purchaseItems;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setPurchaseItems(Map<String, Integer> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }
}
