package com.techu.apitechudb.models;

public class ProductModel {

    public String id;
    public String desc;
    public int price;

    public ProductModel(String id, String desc, int price) {
        this.id = id;
        this.desc = desc;
        this.price = price;
    }

    public ProductModel() {

    }

    public String getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }

    public int getPrice() {
        return price;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}

